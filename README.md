# pyspark Demo - For Work
## Source files
Everything you need to spin up a VM in Vagrant running pyspark and your own Jupyter Notebook.  

## For Jupyter Notebook
To connect using a browser in your host operating system:  
```
jupyter notebook --ip 0.0.0.0 --port 8888
```  
Then connect to this VM using the following URL:  
```
http://192.168.51.151:8888
```

## Starting the stand-alone Spark Server
Type the following to start the Spark server:  
```
start-master.sh
```  
Then start a worker process:  
```
start-worker.sh spark://vagrant:7077
```  
From the host's web browser, check the web UI to ensure that everything is running:  
```
http://192.168.51.151:8080/
```
To stop the master and worker, type the following:  
```
stop-worker.sh
stop-master.sh

# or you can stop both by running this
stop-all.sh
```
## Submitting a Test Job using pyspark in Jupyter Notebook
In Jupyter Notebook, write the following sample code to calculate PI:
```
import findspark
findspark.init()

import pyspark
import random

sc = pyspark.SparkContext(appName="Pi")
num_samples = 1000000000

def inside(p):
    x, y = random.random(), random.random()
    return x*x + y*y < 1

count = sc.parallelize(range(0, num_samples)).filter(inside).count()

pi = 4 * count / num_samples
print(pi)

sc.stop()
```
