#!/usr/bin/env bash

###
### Script variables
### 
SPARK_VERSION=spark-3.1.1
SPARK_FILE=${SPARK_VERSION}-bin-hadoop2.7

# install software updates
sudo apt-get update && sudo apt-get -y upgrade

# install Build Essentials and Python3
sudo apt-get install -y build-essential python3 python3-pip

# install Java
sudo apt-get install -y openjdk-8-jdk

# install Spark
wget https://apache.mirror.colo-serv.net/spark/${SPARK_VERSION}/${SPARK_FILE}.tgz
tar xzvf ${SPARK_FILE}.tgz
sudo mv -v ${SPARK_FILE} /opt/
rm -fv ${SPARK_FILE}.tgz
sudo ln -s /opt/${SPARK_FILE} /opt/spark

# Install Jupyter Notebook
pip3 install jupyter

# Install findspark
pip3 install findspark

# modify shell
cat <<- "EOF" >> ${HOME}/.bashrc

# for Spark
export SPARK_HOME=/opt/spark
export PATH=$SPARK_HOME/bin:$SPARK_HOME/sbin:$PATH

# for Python
alias python=python3
export PYSPARK_PYTHON=python3

# for Jupyter (PIP3)
export PATH=/home/vagrant/.local/bin:$PATH
EOF
